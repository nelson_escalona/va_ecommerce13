from odoo.addons.website_sale.controllers.main import WebsiteSale
from odoo.http import request
import logging
import json
_logger = logging.getLogger(__name__)
from odoo import fields, http, _



class VAWebsiteSale(WebsiteSale):
    def _get_mandatory_shipping_fields(self):
        return ["name", "street", "country_id"]

    def _get_mandatory_billing_fields(self):
        return ["name", "email", "street", "country_id"]

    def values_postprocess(self, order, mode, values, errors, error_msg):
        new_values = {}
        authorized_fields = request.env['ir.model']._get('res.partner')._get_form_writable_fields()
        for k, v in values.items():
            # don't drop empty value, it could be a field to reset
            if k == 'municipality_id' and v is not None:
                new_values[k] = v
                city = request.env['res.country.state.municipality'].search([('id', '=', v)])
                if city:
                    new_values['city'] = city.name
                else:
                    new_values['city'] = False

            if k in authorized_fields and v is not None:
                new_values[k] = v
            else:  # DEBUG ONLY
                if k not in ('field_required', 'partner_id', 'callback', 'submitted'): # classic case
                    _logger.debug("website_sale postprocess: %s value has been dropped (empty or not writable)" % k)

        new_values['team_id'] = request.website.salesteam_id and request.website.salesteam_id.id
        new_values['user_id'] = request.website.salesperson_id and request.website.salesperson_id.id

        if request.website.specific_user_account:
            new_values['website_id'] = request.website.id

        if mode[0] == 'new':
            new_values['company_id'] = request.website.company_id.id

        lang = request.lang.code if request.lang.code in request.website.mapped('language_ids.code') else None
        if lang:
            new_values['lang'] = lang
        if mode == ('edit', 'billing') and order.partner_id.type == 'contact':
            new_values['type'] = 'other'
        if mode[1] == 'shipping':
            new_values['parent_id'] = order.partner_id.commercial_partner_id.id
            new_values['type'] = 'delivery'

        return new_values, errors, error_msg

    @http.route(['/shop/state_infos'], type='json', auth="public", methods=['POST'],
                website=True)
    def state_infos(self, current_country, mode, **kw):
        state = []

        if mode == 'shipping':
            delivery_carriers = request.env['delivery.carrier'].sudo().search([('website_published', '=', True)])
            for carrier in delivery_carriers:
                if not carrier.state_ids:
                    state = []
                    break

                for m in carrier.state_ids:
                    if m.country_id.id == int(current_country or -1):
                        state.append({'id': m.id, 'name': m.name})
        else:
            all_state = request.env['res.country.state'].sudo().search(
                [('country_id', '=', int(current_country or -1))])
            for m in all_state:
                if m.country_id.id == int(current_country or -1):
                    state.append({'id': m.id, 'name': m.name})

        return state

    @http.route(['/shop/municipality_infos'], type='json', auth="public", methods=['POST'],
                website=True)
    def municipality_infos(self, current_state, mode, **kw):
        municipality = []

        if mode == 'shipping':
            delivery_carriers = request.env['delivery.carrier'].sudo().search([('website_published', '=', True)])
            for carrier in delivery_carriers:
                if not carrier.municipality_ids:
                    municipality = []
                    break

                for m in carrier.municipality_ids:
                    if m.state_id.id == int(current_state or -1):
                        municipality.append({'id': m.id, 'name': m.name})
        else:
            all_municipality = request.env['res.country.state.municipality'].sudo().search(
                [('state_id', '=', int(current_state or -1))])
            for m in all_municipality:
                if m.state_id.id == int(current_state or -1):
                    municipality.append({'id': m.id, 'name': m.name})

        return municipality

    @http.route(['/delivery_infos'], type='http', auth="public",
                website=True)
    def delivery_infos(self, **kw):
        delivery_carriers = request.env['delivery.carrier'].sudo().search([('website_published', '=', True)])
        for carrier in delivery_carriers:
            states_list = []
            if carrier.state_ids:
                for st in carrier.state_ids:
                    municipaly_list = []
                    for m in carrier.municipality_ids.filtered(lambda m: m.state_id.id == st.id):
                        municipaly_list.append({'name': m.name})


                    state = {
                        'id': st.id,
                        'name': st.name,
                        'municipaly': municipaly_list
                    }
                    states_list.append(state)

            values = {
                'states': states_list
            }

        return request.render('va_ecommerce13.delivery_infos', values)

    @http.route(['/shop/cart/update_json'], type='json', auth="public", methods=['POST'], website=True, csrf=False)
    def cart_update_json(self, product_id, line_id=None, add_qty=None, set_qty=None, display=True):
        """This route is called when changing quantity from the cart or adding
        a product from the wishlist."""
        order = request.website.sale_get_order(force_create=1)
        if order.state != 'draft':
            request.website.sale_reset()
            return {}

        if set_qty == 0 or self._check_product_cart_rules(product_id):
            value = order._cart_update(product_id=product_id, line_id=line_id, add_qty=add_qty, set_qty=set_qty)

            if not order.cart_quantity:
                request.website.sale_reset()
                return value

            order = request.website.sale_get_order()
            value['cart_quantity'] = order.cart_quantity

            if not display:
                return value

            value['website_sale.cart_lines'] = request.env['ir.ui.view'].render_template("website_sale.cart_lines", {
                'website_sale_order': order,
                'date': fields.Date.today(),
                'suggested_products': order._cart_accessories()
            })
            value['website_sale.short_cart_summary'] = request.env['ir.ui.view'].render_template(
                "website_sale.short_cart_summary", {
                    'website_sale_order': order,
                })
            return value
        else:
            value = {
                'rules': self._literal_category_product_rules(product_id)
            }

            return value

    @http.route(['/shop/checkout'], type='http', auth="public", website=True, sitemap=False)
    def checkout(self, **post):
        cart_rules = self._check_products_cart_rules()
        if not cart_rules['error']:
            order = request.website.sale_get_order()

            redirection = self.checkout_redirection(order)
            if redirection:
                return redirection

            if order.partner_id.id == request.website.user_id.sudo().partner_id.id:
                return request.redirect('/shop/address')

            for f in self._get_mandatory_billing_fields():
                if not order.partner_id[f]:
                    return request.redirect('/shop/address?partner_id=%d' % order.partner_id.id)

            values = self.checkout_values(**post)

            if post.get('express'):
                return request.redirect('/shop/confirm_order')

            values.update({'website_sale_order': order})

            # Avoid useless rendering if called in ajax
            if post.get('xhr'):
                return 'ok'
            return request.render("website_sale.checkout", values)
        else:
            values = {
                'rules': cart_rules['rules']
            }
            return request.render('va_ecommerce13.product_rules_infos', values)

    @http.route(['/shop/cart/update'], type='http', auth="public", methods=['GET', 'POST'], website=True, csrf=False)
    def cart_update(self, product_id, add_qty=1, set_qty=0, **kw):
        sale_order = request.website.sale_get_order(force_create=True)
        if sale_order.state != 'draft':
            request.session['sale_order_id'] = None
            sale_order = request.website.sale_get_order(force_create=True)

        if set_qty == 0 or self._check_product_cart_rules(product_id):
            product_custom_attribute_values = None
            if kw.get('product_custom_attribute_values'):
                product_custom_attribute_values = json.loads(kw.get('product_custom_attribute_values'))

            no_variant_attribute_values = None
            if kw.get('no_variant_attribute_values'):
                no_variant_attribute_values = json.loads(kw.get('no_variant_attribute_values'))

            sale_order._cart_update(
                product_id=int(product_id),
                add_qty=add_qty,
                set_qty=set_qty,
                product_custom_attribute_values=product_custom_attribute_values,
                no_variant_attribute_values=no_variant_attribute_values
            )

            if kw.get('express'):
                return request.redirect("/shop/checkout?express=1")

            return request.redirect("/shop/cart")
        else:
            values = {
                'rules': self._literal_category_product_rules(product_id)
            }
            return request.render('va_ecommerce13.product_rules_infos', values)

    def _check_products_cart_rules(self):
        rules = ''
        order = request.website.sale_get_order()
        for p in order.order_line.product_id:
            if not self._check_product_categories_rules(p.id):
                rules += self._literal_category_product_rules(p.id)

        error = not rules == ''
        return {
                 'error': error,
                 'rules': rules
               }


    def _check_product_cart_rules(self, product_id):
        return self._check_product_categories_rules(product_id)

    def _get_product_categories(self, product_id):
        product = request.env['product.product'].sudo().search([('id', '=', product_id)])
        if product:
            return product.product_tmpl_id.public_categ_ids
        else:
            return None

    def _check_product_categories_rules(self, product_id):
        product_categories = self._get_product_categories(product_id)
        if product_categories:
            for cat in product_categories:
                if not self._check_product_category_rules(cat.id):
                    return False

            return True
        else:
            return True

    def _check_product_category_rules(self, category_id):
        order = request.website.sale_get_order()
        rules = request.env['product.category.rule'].sudo().search([('rule_category_id', '=', category_id)])

        if rules:
            for rul in rules:
                total = sum(order.order_line.filtered(lambda r: rul.category_id in r.product_id.product_tmpl_id.public_categ_ids).mapped("product_uom_qty"))
                if not self._evaluate_rule(rul.operator, rul.quantity, total):
                    return False
            return True
        else:
            return True

    def _evaluate_rule(self, operator, quantity, total):
        if operator == '>':
            return total > quantity
        elif operator == '>=':
            return total >= quantity
        elif operator == '<':
            return total < quantity
        elif operator == '<=':
            return total <= quantity
        elif operator == '=':
            return total == quantity
        else:
            return True

    def _literal_category_product_rules(self, product_id):
        result = ''
        product_categories = self._get_product_categories(product_id)
        if product_categories:
            if product_categories:
                for cat in product_categories:
                    result += self._get_category_rules(cat.id) + '<br/>'

        return result

    def _get_category_rules(self, category_id):
        category_name = request.env['product.public.category'].sudo().search([('id', '=', category_id)],
                                                                             limit=1).name
        result = ''
        rules = request.env['product.category.rule'].sudo().search([('rule_category_id', '=', category_id)])
        if rules:
            for r in rules:
                result += self._translate_rule(r) + '<br/>'

        if result != '':
            # result = category_name + ', de las siguientes categorías, debe añadir : ' + result
            result = 'Antes de comprar ' + category_name + ', de las siguientes categorias, debe añadir: <br/> ' + result
        return result

    def _translate_rule(self, rule):
        if rule.operator == '>':
            str_rule = ' Mas de '
        elif rule.operator == '>=':
            str_rule = ' Un mínino de '
        elif rule.operator == '<':
            str_rule = ' Menos de '
        elif rule.operator == '<=':
            str_rule = ' Menor o '
        elif rule.operator == '=':
            str_rule = ''
        else:
            str_rule = ''

        category_name = request.env['product.public.category'].sudo().search([('id', '=', rule.category_id.id)],limit=1).name
        if not category_name:
            category_name = 'Not Available Category'

        # return ' De ' + category_name + ' ' + str_rule + str(rule.quantity)+' productos.'
        return str_rule + str(rule.quantity) + ' ' + category_name.lstrip('-')












