��          �   %   �      @  ?   A  _   �  �   �  O   �     �     �            
        '     -     :     ?     E     J     [     j     {     �  :   �  �   �     b     j  -   �    �  ?   �  a     �   h  M   .     |     �     �  	   �     �     �  	   �     �  	   �     �     �     �  	   	  	   	     	  8   )	  �   b	  	   3
     =
  ;   U
           	                                                                                   
                                ${object.company_id.name} Invoice (Ref ${object.name or 'n/a'}) <font style="color: rgb(255, 0, 0); font-size: 38px;">
						  <u>IMPORTAN!!!</u>
						</font> <span style="font-weight: bolder; font-size: 18px; text-align: center;">
						  <i>Before you start shopping take a look at the list of available delivery locations</i>
						</span>
						<br/> At the moment we are only available to deliver to the following municipalities. Comeback Contact Country Country state Country... Email Municipio... Name Phone Sale Shipping Methods Start Shopping State / Province State / Province... Street 2 Street <span class="d-none d-md-inline"> and Number</span> We are expanding and hope to be the national food providers very soon. Please check this page regularly for updates on new drop off locations. Website Website Product Category Your shipping address will be requested later Project-Id-Version: Odoo Server 13.0
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-06-21 16:43-0400
Last-Translator: 
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: 
Language: es
X-Generator: Poedit 2.3
 ${object.company_id.name} Factura (Ref ${object.name or 'n/a'}) <font style="color: rgb(255, 0, 0); font-size: 38px;">
						  <u>IMPORTANTE!!!</u>
						</font> <span style="font-weight: bolder; font-size: 18px; text-align: center;">
						  <i>Antes de comenzar la compra revise la lista de municipios disponibles para entrega.</i>
						</span>
						<br/> En este momento solo estamos haciendo entregas en los siguientes municipios.  Página de Inicio Contacto País Provincia País Correo Electrónico Municipio Nombre Teléfono Oferta Métodos de envío Comenzar a Compar Provincia Provincia Datos Extras Calle <span class="d-none d-md-inline"> y Número</span> Nos estamos expandiendo y esperamos ser muy pronto los proveedores de alimentos a nivel nacional. Por favor, revise esta página regularmente para conocer las actualizaciones de los nuevos lugares de entrega. Sitio web Categoría del Producto Su dirección de entrega se le solicitará a continuación. 