odoo.define('va_ecommerce.website_sale', function (require) {
'use strict';

//var ajax = require('web.ajax');
//var core = require('web.core');
var publicWidget = require('web.public.widget');
require('website_sale.website_sale');
const wUtils = require("website_sale.utils");

//var _t = core._t;

publicWidget.registry.WebsiteSale.include({
   events: _.extend({}, publicWidget.registry.WebsiteSale.prototype.events,{
      'change select[name="state_id"]': '_onChangeState',
   }),

    _onChangeState: function (ev) {
        var selectMunicipality = $("select[name='state_id']");
        var current_state_id = $("select[name='state_id']").val();
        var selectMunicipality = $("select[name='municipality_id']");
        var current_mode = $("#va_address_mode").val();
        var partner_shipping_id = $("#va_partner_shipping_id").val();

        selectMunicipality.html('<option value="">Municipality...</option>');

           this._rpc({
                /*model: 'res.country.state.municipality',
                method: 'get_website_sale_municipality',
                kwargs: {
                    current_state: current_state_id,
                    mode: current_mode,
                }*/
                route: "/shop/municipality_infos",
                params: {
                    mode: current_mode,
                    current_state: current_state_id,
                },
            }).then(function (result) {
               if (result.length) {
                    selectMunicipality.html('');
                    _.each(result, function (x) {
                        var opt = $('<option>').text(x.name)
                            .attr('value', x.id);
                        selectMunicipality.append(opt);
                    });
                    selectMunicipality.parent('div').show();
                }
            });
    },

    _changeCountry: function () {
           this._super.apply(this, arguments);
           var current_country_id = $("select[name='country_id']").val();
           var selectState = $("select[name='state_id']");
           var current_state_id = $("select[name='state_id']").val();
           var current_mode = $("#va_address_mode").val();

           this._rpc({
                /*model: 'res.country.state',
                method: 'get_website_sale_state',
                kwargs: {
                    current_country: current_country_id,
                    mode: current_mode,
                }*/
                route: "/shop/state_infos",
                params: {
                    mode: current_mode,
                    current_country: current_country_id,
                },
            }).then(function (states) {
               if (current_country_id == ""){
                  $("select[name='municipality_id']").html('<option value="">Municipality...</option>');
               }

               selectState.html('<option value="">State / Province...</option>');
               if (states.length) {
                   //selectState.html('');
                   _.each(states, function (st) {
                        var opt = $('<option>').text(st.name)
                            .attr('value', st.id);
                        selectState.append(opt);
                    });

                    if (current_state_id){
                       $("select[name='state_id']").val(current_state_id).change();
                    }else{
                       $("select[name='state_id']").change();
                    }

                    selectState.parent('div').show();
                }
            });

    },

});

return publicWidget.registry.WebsiteSaleVAEcommerce;
});