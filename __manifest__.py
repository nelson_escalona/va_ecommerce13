# -*- coding: utf-8 -*-
{
    'name': "VA ECommerce",
    'version': "1.0",
    'summary': "VA ECommerce",

    'author': "VoltusAutomation, KRKA",
    'website': "http://www.voltusautomation.com/",
    'category': "Enterprise Component Management",

    # any module necessary for this one to work correctly
    'depends': ['base', 'web', 'website', 'website_sale', 'stock', 'delivery', 'account'],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'report/report_sold_out.xml',
        'report/report_product_in_stock.xml',
        'report/system_users_report.xml',
        'views/va_ecommerce_views.xml',
        'views/municipality_views.xml',
        'views/product_category_views.xml',
        'views/va_ecommerce_template.xml',
        'views/assets.xml',
        'views/report/report_views.xml',
        'views/product_views.xml',
        'views/deliveredFormView.xml',
     ],
    
    'application': True,
    'installable': True,
}
