from odoo import api, fields, models, tools, _


class ProductPublicCategory(models.Model):
    _inherit = "product.public.category"
    rule_ids = fields.One2many('product.category.rule', 'rule_category_id', string='Rules')


class ProductCategoryRule(models.Model):
    _name = "product.category.rule"
    rule_category_id = fields.Many2one('product.public.category', string='Category', required=True)
    category_id = fields.Many2one('product.public.category', string='Category', required=True)
    operator = fields.Selection(string="Operator", selection=[('=', '='), ('<=', '<='), ('>=', '>='), ('<', '<'), ('>', '>')], required=True, default='=')
    quantity = fields.Integer(string="Quantity", required=True)








