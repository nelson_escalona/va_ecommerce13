import datetime
from odoo import models, fields, api, tools
from dateutil.relativedelta import relativedelta

class ReportSoldOut(models.AbstractModel):
    _name = 'report.va_ecommerce13.report_sold_out'

    @api.model
    def _get_report_values(self, docids, data=None):
        sold_out_report = self.env['ir.actions.report']._get_report_from_name('va_ecommerce13.action_report_print_sold_out')

        # start_date = data['form'][0]['start_date']
        # end_date = data['form'][0]['end_date']


        records = self.env['product.product'].search([('virtual_available', '=',0 ), ('product_tmpl_id.type', '=', 'product')])
        docargs = {
           'doc_ids': self.ids,
           'doc_model': sold_out_report.model,
           'docs': records,
           'date': (datetime.datetime.now() - datetime.timedelta(hours=4)).strftime(tools.DEFAULT_SERVER_DATE_FORMAT),
        }

        return docargs
