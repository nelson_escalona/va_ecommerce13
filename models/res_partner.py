from odoo import api, fields, models


class VAPartner(models.Model):
    _inherit = 'res.partner'

    pp = fields.Char()
    municipality_id = fields.Many2one('res.country.state.municipality', string='Municipio')

    @api.onchange('municipality_id')
    def _onchange_municipality_id(self):
        if self.municipality_id:
            self.city = self.municipality_id.name
        elif self._origin:
            self.city = False