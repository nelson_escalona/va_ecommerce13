from odoo import models, fields, api, tools
import pytz
import datetime

class product_municipaly_report(models.Model):
    _name = 'product.sent.summary.report'
    _auto = False
    state = fields.Char(string='Provincia')
    municipaly = fields.Char(string='Municipio')
    product = fields.Char(string='Producto', translate=True)
    nro_factura = fields.Char(string='Nro Factura')
    date_order = fields.Datetime(string="Fecha")
    quantity = fields.Float(string='Cantidad')

    def _query(self):
        return """SELECT st.name as state, m.name as municipaly,  am.name as nro_factura, CASE tr.value WHEN '' THEN tr.src ELSE tr.value END as product, so.date_order ::TIMESTAMP - '4 hr'::INTERVAL as date_order, SUM(ol.product_uom_qty) as quantity, min(so.id) as id
         FROM public.sale_order so
             INNER JOIN public.sale_order_line ol ON so.id = ol.order_id 
             INNER JOIN public.res_partner p ON so.partner_shipping_id = p.id
             INNER JOIN public.res_country_state_municipality m ON p.municipality_id = m.id
             INNER JOIN public.res_country_state st ON m.state_id = st.id
             INNER JOIN public.product_product prod ON ol.product_id = prod.id
             INNER JOIN public.product_template pt ON prod.product_tmpl_id = pt.id
             INNER JOIN public.ir_translation tr ON pt.id = tr.res_id AND tr.name = 'product.template,name' AND tr.lang = 'es_ES'
             INNER JOIN account_move am ON am.name in (SELECT am2.name
                            FROM sale_order so2
                                JOIN sale_order_line sol2 ON sol2.order_id = so2.id
                                JOIN sale_order_line_invoice_rel soli_rel2 ON soli_rel2.order_line_id = sol2.id
                                JOIN account_move_line aml2 ON aml2.id = soli_rel2.invoice_line_id
                                JOIN account_move am2 ON am2.id = aml2.move_id
                            WHERE so2.name = so.name
                            GROUP BY am2.name) 
          WHERE so.state = 'sale' AND pt.type = 'product' 
          GROUP BY st.name, municipaly, nro_factura, product, date_order
          ORDER BY st.name, municipaly, nro_factura
          """

    def init(self):
        tools.drop_view_if_exists(self.env.cr, self._table)
        self.env.cr.execute("""CREATE or REPLACE VIEW %s as (%s)""" % (self._table, self._query()))