# -*- coding: utf-8 -*-

from odoo import api, fields, models, tools, SUPERUSER_ID, _
from odoo.tools.misc import get_lang
from odoo.http import request


class VASaleOrderLine(models.Model):
    _inherit = 'sale.order.line'
    cost = fields.Float('Cost', compute='_get_cost', store=True)

    @api.depends('product_id')
    def _get_cost(self):
        for line in self:
            if line.product_id:
                line.cost = line.product_id.standard_price * line.product_uom_qty
            else:
                line.cost = 0


class VAProduct(models.Model):
    _name = "va.product"
    _inherit = "product.product"

    @api.model
    def print_sold_out(self):
        rec = self.browse()
        data = {}
        data['form'] = rec

        return self.env.ref('va_ecommerce13.action_report_print_sold_out').report_action(self, data)

    @api.model
    def print_product_in_stock(self):
        rec = self.browse()
        data = {}
        data['form'] = rec

        return self.env.ref('va_ecommerce13.action_report_print_product_in_stock').report_action(self, data)


class VAWebsite(models.Model):
    _inherit = 'website'

    def sale_get_order(self, force_create=False, code=None, update_pricelist=False, force_pricelist=False):
        """ Return the current sales order after mofications specified by params.
        :param bool force_create: Create sales order if not already existing
        :param str code: Code to force a pricelist (promo code)
                         If empty, it's a special case to reset the pricelist with the first available else the default.
        :param bool update_pricelist: Force to recompute all the lines from sales order to adapt the price with the current pricelist.
        :param int force_pricelist: pricelist_id - if set,  we change the pricelist with this one
        :returns: browse record for the current sales order
        """
        self.ensure_one()
        partner = self.env.user.partner_id
        sale_order_id = request.session.get('sale_order_id')
        check_fpos = False
        if not sale_order_id and not self.env.user._is_public():
            last_order = partner.last_website_so_id
            if last_order:
                available_pricelists = self.get_pricelist_available()
                # Do not reload the cart of this user last visit if the cart uses a pricelist no longer available.
                sale_order_id = last_order.pricelist_id in available_pricelists and last_order.id
                check_fpos = True

        # Test validity of the sale_order_id
        # sale_order = self.env['sale.order'].with_context(force_company=request.website.company_id.id).sudo().browse(sale_order_id).exists() if sale_order_id else None
        sale_order = self.env['sale.order'].with_context(force_company=request.website.company_id.id).sudo().search([('id', '=', sale_order_id),('website_id', '=', request.website.id)])

        # Do not reload the cart of this user last visit if the Fiscal Position has changed.
        if check_fpos and sale_order:
            fpos_id = (
                self.env['account.fiscal.position'].sudo()
                .with_context(force_company=sale_order.company_id.id)
                .get_fiscal_position(sale_order.partner_id.id, delivery_id=sale_order.partner_shipping_id.id)
            )
            if sale_order.fiscal_position_id.id != fpos_id:
                sale_order = None

        if not (sale_order or force_create or code):
            if request.session.get('sale_order_id'):
                request.session['sale_order_id'] = None
            return self.env['sale.order']

        if self.env['product.pricelist'].browse(force_pricelist).exists():
            pricelist_id = force_pricelist
            request.session['website_sale_current_pl'] = pricelist_id
            update_pricelist = True
        else:
            pricelist_id = request.session.get('website_sale_current_pl') or self.get_current_pricelist().id

        if not self._context.get('pricelist'):
            self = self.with_context(pricelist=pricelist_id)

        # cart creation was requested (either explicitly or to configure a promo code)
        if not sale_order:
            # TODO cache partner_id session
            pricelist = self.env['product.pricelist'].browse(pricelist_id).sudo()
            so_data = self._prepare_sale_order_values(partner, pricelist)
            sale_order = self.env['sale.order'].with_context(force_company=request.website.company_id.id).with_user(SUPERUSER_ID).create(so_data)

            # set fiscal position
            if request.website.partner_id.id != partner.id:
                sale_order.onchange_partner_shipping_id()
            else: # For public user, fiscal position based on geolocation
                country_code = request.session['geoip'].get('country_code')
                if country_code:
                    country_id = request.env['res.country'].search([('code', '=', country_code)], limit=1).id
                    fp_id = request.env['account.fiscal.position'].sudo().with_context(force_company=request.website.company_id.id)._get_fpos_by_region(country_id)
                    sale_order.fiscal_position_id = fp_id
                else:
                    # if no geolocation, use the public user fp
                    sale_order.onchange_partner_shipping_id()

            request.session['sale_order_id'] = sale_order.id

        # case when user emptied the cart
        if not request.session.get('sale_order_id'):
            request.session['sale_order_id'] = sale_order.id

        # check for change of pricelist with a coupon
        pricelist_id = pricelist_id or partner.property_product_pricelist.id

        # check for change of partner_id ie after signup
        if sale_order.partner_id.id != partner.id and request.website.partner_id.id != partner.id:
            flag_pricelist = False
            if pricelist_id != sale_order.pricelist_id.id:
                flag_pricelist = True
            fiscal_position = sale_order.fiscal_position_id.id

            # change the partner, and trigger the onchange
            sale_order.write({'partner_id': partner.id})
            sale_order.with_context(not_self_saleperson=True).onchange_partner_id()
            sale_order.write({'partner_invoice_id': partner.id})
            sale_order.onchange_partner_shipping_id() # fiscal position
            sale_order['payment_term_id'] = self.sale_get_payment_term(partner)

            # check the pricelist : update it if the pricelist is not the 'forced' one
            values = {}
            if sale_order.pricelist_id:
                if sale_order.pricelist_id.id != pricelist_id:
                    values['pricelist_id'] = pricelist_id
                    update_pricelist = True

            # if fiscal position, update the order lines taxes
            if sale_order.fiscal_position_id:
                sale_order._compute_tax_id()

            # if values, then make the SO update
            if values:
                sale_order.write(values)

            # check if the fiscal position has changed with the partner_id update
            recent_fiscal_position = sale_order.fiscal_position_id.id
            # when buying a free product with public user and trying to log in, SO state is not draft
            if (flag_pricelist or recent_fiscal_position != fiscal_position) and sale_order.state == 'draft':
                update_pricelist = True

        if code and code != sale_order.pricelist_id.code:
            code_pricelist = self.env['product.pricelist'].sudo().search([('code', '=', code)], limit=1)
            if code_pricelist:
                pricelist_id = code_pricelist.id
                update_pricelist = True
        elif code is not None and sale_order.pricelist_id.code and code != sale_order.pricelist_id.code:
            # code is not None when user removes code and click on "Apply"
            pricelist_id = partner.property_product_pricelist.id
            update_pricelist = True

        # update the pricelist
        if update_pricelist:
            request.session['website_sale_current_pl'] = pricelist_id
            values = {'pricelist_id': pricelist_id}
            sale_order.write(values)
            for line in sale_order.order_line:
                if line.exists():
                    sale_order._cart_update(product_id=line.product_id.id, line_id=line.id, add_qty=0)

        return sale_order

class VASaleOrder(models.Model):
    _inherit = 'sale.order'

    send_to_cuba = fields.Boolean(string='Enviado a Cuba')
    delivered = fields.Boolean(string='Entregado', default=False)
    invoice_name = fields.Char(string='Invoice Name', readonly=True, compute='_search_invoice')

    def _search_invoice(self):
        for order in self:
            self.env.cr.execute("""
                            SELECT am.name
                            FROM sale_order so
                                JOIN sale_order_line sol ON sol.order_id = so.id
                                JOIN sale_order_line_invoice_rel soli_rel ON soli_rel.order_line_id = sol.id
                                JOIN account_move_line aml ON aml.id = soli_rel.invoice_line_id
                                JOIN account_move am ON am.id = aml.move_id
                            WHERE so.name = %s
                            GROUP BY am.name
                        """, (order.name,))

            result = self.env.cr.fetchone()

            if result is None:
                order.invoice_name = ''
            else:
                order.invoice_name = result[0]

            if order.invoice_name == '/':
                order.invoice_name = ''

    payment_name = fields.Char(string='Método de Pago ', readonly=True, compute='_search_payment_name')

    def _search_payment_name(self):
        for order in self:
            self.env.cr.execute("""
                                SELECT tr.value
                                  FROM sale_order so
                                  INNER JOIN sale_order_line sol ON sol.order_id = so.id
                                  INNER JOIN sale_order_line_invoice_rel soli_rel ON soli_rel.order_line_id = sol.id
                                  INNER JOIN account_move_line aml ON aml.id = soli_rel.invoice_line_id
                                  INNER JOIN account_move am ON am.id = aml.move_id
                                  INNER JOIN account_invoice_payment_rel apr ON am.id = apr.invoice_id
                                  INNER JOIN account_payment ap ON  ap.id = apr.payment_id  
                                  INNER JOIN account_journal aj ON aj.id = ap.journal_id 
                                  INNER JOIN public.ir_translation tr ON tr.src = aj.name AND tr.lang = 'es_ES'
                                WHERE so.name = %s
                                GROUP BY tr.value
                            """, (order.name,))

            result = self.env.cr.fetchone()

            if result is None:
                order.payment_name = ''
            else:
                order.payment_name = result[0]

    def action_check_delivered(self):
        sale_order_ids = self.env.context.get('active_ids')
        # self.write({'delivered': True})
        mark_view_id = self.env.ref('va_ecommerce13.delivered_form_view').id
        ctx = dict()
        ctx.update({
            'order_ids': sale_order_ids
        })
        return {
            'type': 'ir.actions.act_window',
            'name': 'Opciones de entregado',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'delivered_form_model',
            'views': [(mark_view_id, 'form')],
            'view_id': mark_view_id,
            'target': 'new',
            'context': ctx,
        }

    def action_uncheck_delivered(self):
        self.write({'delivered': False})

    def action_check_sent_to_cuba(self):
        self.write({'send_to_cuba': True})

    def action_uncheck_sent_to_cuba(self):
        self.write({'send_to_cuba': False})

    def action_sent_summary(self):
        sale_order_ids = self.env['sale.order'].browse(self.env.context.get('active_ids'))

        invoice_names = []
        for sale in sale_order_ids:
            invoice_names.append(sale.invoice_name)

        return {
            'type': 'ir.actions.act_window',
            'name': 'Consolidado de envios',
            'res_model': 'product.sent.summary.report',
            'domain': [('nro_factura', 'in', invoice_names)],
            'view_mode': 'pivot',
            'context': {}
        }

    def action_cost_summary(self):
        sale_order_ids = self.env['sale.order'].browse(self.env.context.get('active_ids'))

        invoice_names = []
        for sale in sale_order_ids:
            invoice_names.append(sale.invoice_name)

        return {
            'type': 'ir.actions.act_window',
            'name': 'Consolidado de Costos',
            'res_model': 'product.cost.summary.report',
            'domain': [('nro_factura', 'in', invoice_names)],
            'view_mode': 'pivot',
            'context': {}
        }

    class VAAccountMove(models.Model):
        _inherit = "account.move"

        payment_name = fields.Char(string='Método de Pago ', readonly=True, compute='_search_payment_name')

        def _search_payment_name(self):
            for invoice in self:
                self.env.cr.execute("""
                                        SELECT tr.value
                                          FROM  account_move am
                                          INNER JOIN account_invoice_payment_rel apr ON am.id = apr.invoice_id
                                          INNER JOIN account_payment ap ON  ap.id = apr.payment_id  
                                          INNER JOIN account_journal aj ON aj.id = ap.journal_id 
                                          INNER JOIN public.ir_translation tr ON tr.src = aj.name AND tr.lang = 'es_ES'
                                        WHERE am.id = %s
                                        GROUP BY tr.value
                                    """, (invoice.id,))

                result = self.env.cr.fetchone()

                if result is None:
                    invoice.payment_name = ''
                else:
                    invoice.payment_name = result[0]

        isDUCPay = fields.Boolean(string='Fue Pagada por DUC', readonly=True, compute='_search_is_duc_pay')

        def _search_is_duc_pay(self):
            for invoice in self:
                invoice.isDUCPay = invoice.payment_name.startswith('DUC')

        # def action_invoice_sent(self):
        #     """ Open a window to compose an email, with the edi invoice template
        #         message loaded by default
        #     """
        #     self.ensure_one()
        #     template = self.env.ref('va_ecommerce13.va_email_template_edi_invoice', raise_if_not_found=False)
        #     lang = get_lang(self.env)
        #     if template and template.lang:
        #         lang = template._render_template(template.lang, 'account.move', self.id)
        #     else:
        #         lang = lang.code
        #     compose_form = self.env.ref('account.account_invoice_send_wizard_form', raise_if_not_found=False)
        #     ctx = dict(
        #         default_model='account.move',
        #         default_res_id=self.id,
        #         # For the sake of consistency we need a default_res_model if
        #         # default_res_id is set. Not renaming default_model as it can
        #         # create many side-effects.
        #         default_res_model='account.move',
        #         default_use_template=bool(template),
        #         default_template_id=template and template.id or False,
        #         default_composition_mode='comment',
        #         mark_invoice_as_sent=True,
        #         custom_layout="mail.mail_notification_paynow",
        #         model_description=self.with_context(lang=lang).type_name,
        #         force_email=True
        #     )
        #     return {
        #         'name': _('Send Invoice'),
        #         'type': 'ir.actions.act_window',
        #         'view_type': 'form',
        #         'view_mode': 'form',
        #         'res_model': 'account.invoice.send',
        #         'views': [(compose_form.id, 'form')],
        #         'view_id': compose_form.id,
        #         'target': 'new',
        #         'context': ctx,
        #     }

class VAProductChangeQuantity(models.TransientModel):
    _inherit = "stock.change.product.qty"

    add_qty = fields.Float('Adicionar', required=True)

    @api.onchange('product_id')
    def _onchange_product_id(self):
        self.new_quantity = self.product_id.qty_available
        self.add_qty = self.product_id.virtual_available
        # self.add_qty = 0  # self.product_id.virtual_available

    def change_product_qty(self):
        """ Changes the Product Quantity by creating/editing corresponding quant.
        """
        warehouse = self.env['stock.warehouse'].search(
            [('company_id', '=', self.env.company.id)], limit=1
        )
        # Before creating a new quant, the quand `create` method will check if
        # it exists already. If it does, it'll edit its `inventory_quantity`
        # instead of create a new one.
        self.env['stock.quant'].with_context(inventory_mode=True).create({
            'product_id': self.product_id.id,
            'location_id': warehouse.lot_stock_id.id,
            # 'inventory_quantity': self.product_id.qty_available + (self.add_qty - self.product_id.sales_count),
            'inventory_quantity': self.product_id.qty_available + (self.add_qty - self.product_id.virtual_available),
        })
        return {'type': 'ir.actions.act_window_close'}


class VAProductTemplate(models.Model):
    _inherit = "product.template"

    inexhaustible = fields.Boolean('Producto inagotable', default=False)

    @api.onchange('inexhaustible')
    def _onchange_inexhaustible(self):
        if self.inexhaustible:
            qty = 9999999
        else:
            qty = 0

        product = self.env['product.product'].search(
            [('product_tmpl_id', '=', self.id.origin)], limit=1
        )

        warehouse = self.env['stock.warehouse'].search(
            [('company_id', '=', self.env.company.id)], limit=1
        )

        self.env['stock.quant'].with_context(inventory_mode=True).create({
            'product_id': product.id,
            'location_id': warehouse.lot_stock_id.id,
            'inventory_quantity': product.qty_available + (qty - product.virtual_available),
        })



