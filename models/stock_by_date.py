
from odoo import models, fields, api, tools

class ReportStockByDate(models.TransientModel):
    _name = 'report_stock_by_date'
    start_date = fields.Date('Fecha de Inicio', default=fields.Date.context_today)
    end_date = fields.Date('Fecha Final', default=fields.Date.context_today)