
from odoo import models, fields, api, tools


class deliveredFormModel(models.TransientModel):
    _name = 'delivered_form_model'
    mark = fields.Selection(selection=[('only_mark', 'Solo marcar'), ('mark_and_notify', 'Marcar y enviar correo')], required=True, default='only_mark')

    def actionMark(self):
        sale_order_ids = self.env['sale.order'].browse(self.env.context.get('order_ids'))
        if self.mark == 'only_mark':
            for order in sale_order_ids:
                order.write({'delivered': True})
        else:
            for order in sale_order_ids:
                order.write({'delivered': True})
                self.sendNotificationMail(order)

    def sendNotificationMail(self, order):
        company = self.env.user.company_id
        render_context = {
            'order_nro': order.name,
            'company': company,

        }
        template = self.env.ref('va_ecommerce13.email_template_delivered_notification')
        mail_body = template.render(render_context, engine='ir.qweb', minimal_qcontext=True)
        #  mail_body = self.env['mail.thread']._replace_local_links(mail_body)
        mail = self.env['mail.mail'].create({
            'subject': order.name,
            'email_from': self.env.company.email,
            'email_to': order.partner_id.email,
            'body_html': mail_body,
        })
        mail.send()