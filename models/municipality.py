from odoo import api, fields, models
import json


class CountryStateMunicipality(models.Model):
    _description = "Country state municipality"
    _name = 'res.country.state.municipality'

    name = fields.Char(string='Municipality Name', required=True,
               help='Administrative divisions of a State. E.g. Cifuentes, Villa Clara, Cuba')
    # code = fields.Char(string='State Code', help='The state code.', required=True)
    state_id = fields.Many2one('res.country.state', string='State', required=True)
    partner_id = fields.One2many('res.partner', 'municipality_id', string='Partner')

    @api.model
    def get_website_sale_municipality(self, current_state, mode='billing'):
        municipality = []

        if mode == 'shipping':
            delivery_carriers = self.env['delivery.carrier'].sudo().search([('website_published', '=', True)])
            for carrier in delivery_carriers:
                if not carrier.municipality_ids:
                    municipality = []
                    break

                for m in carrier.municipality_ids:
                    if m.state_id.id == int(current_state or -1):
                        municipality.append({'id': m.id, 'name': m.name})
        else:
            all_municipality = self.env['res.country.state.municipality'].sudo().search([('state_id', '=', int(current_state or -1))])
            for m in all_municipality:
                if m.state_id.id == int(current_state or -1):
                    municipality.append({'id': m.id, 'name': m.name})

        return municipality

class CountryState(models.Model):
    _name = 'res.country.state'
    _inherit = 'res.country.state'

    municipality_ids = fields.One2many('res.country.state.municipality', 'state_id', string='Municipality')

    @api.model
    def get_website_sale_state(self, current_country, mode='billing'):
        state = []

        if mode == 'shipping':
            delivery_carriers = self.env['delivery.carrier'].sudo().search([('website_published', '=', True)])
            for carrier in delivery_carriers:
                if not carrier.state_ids:
                    state = []
                    break

                for m in carrier.state_ids:
                    if m.country_id.id == int(current_country or -1):
                        state.append({'id': m.id, 'name': m.name})
        else:
            all_state = self.env['res.country.state'].sudo().search(
                [('country_id', '=', int(current_country or -1))])
            for m in all_state:
                if m.country_id.id == int(current_country or -1):
                    state.append({'id': m.id, 'name': m.name})

        return state






class DeliveryCarrier(models.Model):
    _name = 'delivery.carrier'
    _inherit = 'delivery.carrier'

    municipality_ids = fields.Many2many('res.country.state.municipality', 'delivery_carrier_municipality_rel', 'carrier_id', 'municipality_id', 'Municipality')

    @api.onchange('municipality_ids')
    def onchange_municipality(self):
        self.state_ids = [(6, 0, self.state_ids.ids + self.municipality_ids.mapped('state_id.id'))]
        self.country_ids = [(6, 0, self.country_ids.ids + self.municipality_ids.mapped('state_id.country_id.id'))]

    def _match_address(self, partner):
        self.ensure_one()
        if self.country_ids and partner.country_id not in self.country_ids:
            return False
        if self.state_ids and partner.state_id not in self.state_ids:
            return False
        if self.zip_from and (partner.zip or '').upper() < self.zip_from.upper():
            return False
        if self.zip_to and (partner.zip or '').upper() > self.zip_to.upper():
            return False
        if self.municipality_ids and partner.municipality_id not in self.municipality_ids:
            return False
        return True



