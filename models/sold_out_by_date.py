
from odoo import models, fields, api, tools
from datetime import datetime, date

class ReportSoldOutByDate(models.TransientModel):
    _name = 'report_sold_out_by_date'
    start_date = fields.Date('Fecha de Inicio', default=date.today())
    end_date = fields.Date('Fecha Final', default=fields.date.today())

    @api.model
    def print_sold_out(self, data):
        rec = self.browse(data)
        data = {'form': rec.read(['start_date', 'end_date'])}

        return self.env.ref('va_ecommerce13.action_report_print_sold_out').report_action(self, data)