import datetime
from odoo import models, fields, api, tools

class SystemUsersReport(models.AbstractModel):
    _name = 'report.va_ecommerce13.va_system_users_report'

    @api.model
    def _get_report_values(self, docids, data=None):
        system_users_report = self.env['ir.actions.report']._get_report_from_name('va_ecommerce13.action_print_va_system_users_report')
        records = self.env['va.system.user'].search([], order="name asc")

        docargs = {
           'doc_ids': self.ids,
           'doc_model': system_users_report.model,
           'docs': records,
           'date': (datetime.datetime.now() - datetime.timedelta(hours=4)).strftime(tools.DEFAULT_SERVER_DATE_FORMAT),
        }

        return docargs
