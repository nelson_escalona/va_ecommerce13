from odoo import models, fields, api, tools
import pytz
import datetime

class product_municipaly_report(models.Model):
    _name = 'product.municipaly.report'
    _auto = False
    state = fields.Char(string='Provincia')
    municipaly = fields.Char(string='Municipio')
    product = fields.Char(string='Producto', translate=True)
    nro_orden = fields.Char(string='Nro Orden')
    date_order = fields.Datetime(string="Fecha")
    quantity = fields.Float(string='Cantidad')

    def _query(self):
         # CONCAT(st.name, '|', m.name) as municipaly
         return """SELECT st.name as state, m.name as municipaly,  so.name as nro_orden, CASE tr.value WHEN '' THEN tr.src ELSE tr.value END as product, so.date_order ::TIMESTAMP - '4 hr'::INTERVAL as date_order, SUM(ol.product_uom_qty) as quantity, min(so.id) as id
         FROM public.sale_order so
             INNER JOIN public.sale_order_line ol ON so.id = ol.order_id 
             INNER JOIN public.res_partner p ON so.partner_shipping_id = p.id
             INNER JOIN public.res_country_state_municipality m ON p.municipality_id = m.id
             INNER JOIN public.res_country_state st ON m.state_id = st.id
             INNER JOIN public.product_product prod ON ol.product_id = prod.id
             INNER JOIN public.product_template pt ON prod.product_tmpl_id = pt.id
             INNER JOIN public.ir_translation tr ON pt.id = tr.res_id AND tr.name = 'product.template,name' AND tr.lang = 'es_ES'
          WHERE so.state = 'sale' AND pt.type = 'product' 
          GROUP BY st.name, municipaly, nro_orden, product, date_order
          ORDER BY st.name, municipaly, nro_orden
          """

    def init(self):
        tools.drop_view_if_exists(self.env.cr, self._table)
        self.env.cr.execute("""CREATE or REPLACE VIEW %s as (%s)""" % (self._table, self._query()))