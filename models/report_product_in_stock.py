import datetime
from datetime import date
from odoo import models, fields, api, tools
from dateutil.relativedelta import relativedelta

class ReportProductInStock(models.AbstractModel):
    _name = 'report.va_ecommerce13.report_product_in_stock'

    @api.model
    def _get_report_values(self, docids, data=None):
        product_in_stock_report = self.env['ir.actions.report']._get_report_from_name('va_ecommerce13.action_report_print_product_in_stock')
        records = self.env['product.product'].search([('virtual_available', '>',0 ), ('product_tmpl_id.type', '=', 'product')])

        docargs = {
           'doc_ids': self.ids,
           'doc_model': product_in_stock_report.model,
           'docs': records,
           'date': (datetime.datetime.now() - datetime.timedelta(hours=4)).strftime(tools.DEFAULT_SERVER_DATE_FORMAT),
        }

        return docargs
