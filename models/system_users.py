from odoo import models, fields, api, tools

class system_user(models.Model):
    _name = 'va.system.user'
    _auto = False
    name = fields.Char(string='Name')
    email = fields.Char(string="Email")
    phone = fields.Char(string='Phone')

    def _query(self):
        return """SELECT min(name) as name, email, min(phone) as phone, min(id) as id  
                  FROM res_partner where active = true and not (email is null)
                  GROUP BY email
                  ORDER BY name
          """

    def init(self):
        tools.drop_view_if_exists(self.env.cr, self._table)
        self.env.cr.execute("""CREATE or REPLACE VIEW %s as (%s)""" % (self._table, self._query()))


    @api.model
    def print_system_user(self):
        rec = self.browse()
        data = {}
        data['form'] = rec

        return self.env.ref('va_ecommerce13.action_print_va_system_users_report').report_action(self, data)

